# Cocus-Tests

This is a project for the recruitment tests from Cocus Portugal.

The exercise is divided into:

- [Nodejs app fetching data from github](github-app)

- [Note taking app](note-app)

### Running the App(s)

Its quite easy to boot up the app with docker (compose).

Sample configuration is already provided (and a github token - set to expire in a month (circa first week of December 2022)).

```sh
 docker-compose -p cocus up 
```

The applications can be accessed via:
- [Github-App](http://localhost:8081/api/search/github/<username>)
- [Note-App](http://localhost:8082/)


Locally, the apps can be run via:

For the github app:
A sample environment file is provided [here](github-app/.env.sample)

```sh
touch .env
cd github-app 
npm run start:dev
```

For the note app:

```sh
cd node-app
npm start
```

