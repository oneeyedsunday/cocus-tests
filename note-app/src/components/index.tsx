import React, { FC, useContext, useEffect, useRef } from "react";
import ReactMarkdown from 'react-markdown';
import TextareaAutosize from 'react-textarea-autosize';
import remarkGfm from 'remark-gfm';
import { FaTrashAlt, FaPen, FaSistrix, FaFileImport, FaFileExport } from "react-icons/fa";
import Context, { Note as INote } from "../contexts";

const NoteDisplay: FC<{ item: INote }> = (props) => {
	const { text, date, id, title, tags = [] } = props.item;
	const { handleDelete, handleEdit } = useContext(Context);
	return (
		<div className="note">
			<span className="text--italic">{title || '<Untitled>'}</span>
            <div className="note--contents">
            <div className="note--tag--wrapper">
                Tags: {
                    tags?.length ?
                    tags.map((t) => <span key={t} className="note--tag">{t}</span>)
                    : <span className="text--italic text--sm">&lt;Untagged&gt;</span>
                }
            </div>
            <ReactMarkdown
                className="note--text"
                remarkPlugins={[remarkGfm]}
                linkTarget="_blank"
                skipHtml
            >
                {text}
            </ReactMarkdown>
            </div>
            
			<div className="note-footer">
				<small>{date.toLocaleString('en-US')}</small>
				<div className="right">
					<FaPen className="edit-icon" onClick={() => handleEdit(id)} />
					<FaTrashAlt
						className="delete-icon"
						size="1.6rem"
						onClick={() => handleDelete(id)}
					/>
				</div>
			</div>
		</div>
	);
};

const AddNote = () => {
	const {
        noteText, noteTitle, noteTags, characterCount, selectedNote,
        handleChange, handleSave, handleCancel
    } =	useContext(Context);

    return (
        <div className="note new-note">
            <textarea
                className="note--title"
				cols={10}
				rows={1}
				placeholder="Add a note title..."
                value={noteTitle}
				onChange={(e) => handleChange('note.title', e)}
			></textarea>
            <textarea
                className="note--title"
				cols={10}
				rows={1}
				placeholder="Tags (comma separated): "
                value={noteTags}
				onChange={(e) => handleChange('note.tags', e)}
			></textarea>
            <TextareaAutosize
				cols={10}
				rows={8}
				placeholder="Type to add a note..."
                value={noteText}
                minRows={8}
				onChange={(e) => handleChange('note.text', e)}
			></TextareaAutosize>
            <div className="note-footer">
				<small>{characterCount - noteText.length} Remaining</small>


                <div>
                    <button
                        style={{marginRight: 10}}
                        className="btn btn--delete"
                        onClick={handleCancel}
                    >
                        Cancel
                    </button>
                    <button
                        className="btn btn--save"
                        onClick={() => handleSave(noteText, noteTitle, noteTags)} disabled={!(noteText.trim().length && noteTitle.trim().length)}>
                        {selectedNote?.id ? "Edit" : "Save"}
                    </button>
                </div>
                
			</div>
        </div>
    )
};

const Header = () => {
	const { handleDarkMode, handleDataExport, handleDataImport } = useContext(Context);
	return (
		<div className="header">
			<h1>Notes</h1>
            <div className="header--actions--wrapper">
            <button className="btn btn--save btn--import" title="Import Data">
                <label htmlFor="dataImport">
                    <FaFileImport />
                </label>
                <input
                    id="dataImport"
                    type="file"
                    style={{opacity: 0}}
                    accept="application/json"
                    onChange={(e) => handleDataImport(e.target.files)}
                />
				
			</button>
            <button className="btn btn--save" onClick={handleDataExport} title="Export Data">
				<FaFileExport />
			</button>
			<button className="btn btn--save" onClick={handleDarkMode} title="Toggle Dark Mode">
				Toggle Dark Mode
			</button>
            </div>
		</div>
	);
};

const sortNotesByDateDescending = (a: INote, b: INote): number => {
    return a.date.valueOf() > b.date.valueOf() ? -1 : 1;
};

const noteFilterFn = (item: INote, searchValue: string, selected: INote | null) => {
    if (selected && selected.id === item.id) return false;

    if (!searchValue) return true;

    return item.text.toLowerCase().includes(searchValue) || item.title.toLowerCase().includes(searchValue);    
};

const NotesList = () => {
	let { notes, searchText, selectedNote } = useContext(Context);
	const searchValue = searchText.toLowerCase();

    const filteredSortedNotes = notes
        .filter((item) => noteFilterFn(item, searchValue, selectedNote))
        .sort(sortNotesByDateDescending);

	return (
		<div className="note-list">
            {
                !searchValue && <AddNote />
            }
			
			{
                filteredSortedNotes
                .map((note) => (
					<NoteDisplay item={note} key={note.id} />
				))
            }

            {
                !filteredSortedNotes.length && (
                <div className="no--notes">
                    { !searchValue && <p>No notes yet.</p> }
                    { searchValue && <p>No notes Found for "<span className="text--italic">{searchValue}</span>"</p> }
                </div>
                )
            }
		</div>
	);
};

const Search = () => {
	const { searchText, handleSearchText } = useContext(Context);
	return (
		<div className="search">
			<FaSistrix size="1.8rem" />
			<input
				type="text"
				value={searchText}
				onChange={(e) => handleSearchText(e.target.value)}
				placeholder="type to search..."
			/>
		</div>
	);
};

export {
    NotesList,
    Header,
    Search
};
