import React, { useContext } from 'react';
import './App.css';


import { NotesList, Search, Header } from "./components";
import Context from "./contexts";

function App() {
  const { darkMode } = useContext(Context);
	return (
		<div className={darkMode ? "dark-mode" : undefined}>
			<div className="container">
				<Header />
				<Search />
				<NotesList />
			</div>
		</div>
	);
}

export default App;
