import React, { createContext, useState, useEffect, ReactNode, FC } from "react";
import { nanoid } from "nanoid";

export type Note = {
    title: string;
    text: string;
    date: Date;
    id: string;
    tags?: string[];
}

/**
 * @description StringFunc is a function that takes a string as a single parameter with no return
 */
type StringFunc = (arg: string) => void;
/**
 * @description ThreeStringParamsFunc is a function that takes in three strings as parameters with no return
 */
type ThreeStringParamsFunc = (arg1: string, arg2: string, arg3: string) => void;

type DownloadFileFn = ( data: any, fileName: string, fileType: string ) => void;

const NoopStringFunc = (arg: string) => {};
const NoopFunc = () => {};

const NOTES_KEY = "note-app__notes";
const MAX_CHARACTER_COUNT = 500;

type ChangeTypes = 'note.title' | 'note.text' | 'note.tags';

type ChangeHandleFn = (type: ChangeTypes, e: React.ChangeEvent<HTMLTextAreaElement>) => void;

export type NotesAppContext = {
    darkMode: boolean;
    setDarkMode: React.Dispatch<React.SetStateAction<boolean>>;
    handleDarkMode: () => void;
    handleDelete: StringFunc;
    handleEdit: StringFunc;
    handleSearchText: StringFunc;
    handleSave: ThreeStringParamsFunc;
    characterCount: number;
    searchText: string;
    noteText: string;
    noteTitle: string;
    noteTags: string;
    notes: Array<Note>;
    handleChange: ChangeHandleFn;
    handleCancel: () => void;
    handleDataImport: (files: FileList | null) => void;
    handleDataExport: () => void;
    selectedNote: Note | null;
};

const Context = createContext<NotesAppContext>({
    darkMode: false,
    setDarkMode: () => {},
    handleDarkMode: () => {},
    handleDelete: NoopStringFunc,
    handleEdit: NoopStringFunc,
    handleSearchText: NoopStringFunc,
    handleSave: NoopStringFunc,
    handleChange: (type: ChangeTypes, e: any) => {},
    handleCancel: NoopFunc,
    handleDataImport: NoopFunc,
    handleDataExport: NoopFunc,
    characterCount: MAX_CHARACTER_COUNT,
    searchText: '',
    noteText: '',
    noteTitle: '',
    noteTags: '',
    notes: [],
    selectedNote: null,
});

function getNotesFromStorage(): Array<Note> {
    const notesFromStorage = localStorage.getItem(NOTES_KEY);
    

    if (!notesFromStorage) return [];

    try {
        return JSON.parse(notesFromStorage) as Array<Note>;
    } catch (error) {
        console.error('Corrupted storage; resetting...');
        localStorage.removeItem(NOTES_KEY);
        return [];
    }
}

function storeNotesInStorage(notes: Array<Note>): void {
    localStorage.setItem(NOTES_KEY, JSON.stringify(notes));
}

function toStringArray(rawValue: string = '') {
    return rawValue.split(',').filter((tag) => tag.trim().length > 0);
}

export const ContextProvider: FC<{ children: ReactNode }>  = ({ children }) => {
    const prefersDark = window.matchMedia(
        "(prefers-color-scheme: dark)"
      ).matches;
	const [darkMode, setDarkMode] = useState(prefersDark);

    const [notes, setNotes] = useState(getNotesFromStorage());
    const [noteText, setNoteText] = useState("");
    const [noteTitle, setNoteTitle] = useState("");
    const [noteTags, setNoteTags] = useState("");
    const [searchText, setSearchtext] = useState("");
    const [selectedNote, setSelectedNote] = useState<Note | null>(null);

    useEffect(() => {
		storeNotesInStorage(notes);
	}, [notes]);


    function clearNoteEditing() {
        setNoteText("");
        setNoteTitle("");
        setNoteTags("");
    }

    const handleChange: ChangeHandleFn = (type, e) => {
		const value = e.target.value;

        switch (type) {
            case 'note.text':
                if (MAX_CHARACTER_COUNT >= e.target.value?.length) {
                    setNoteText(value);
                }
                break;
            case 'note.title':
                setNoteTitle(value);
                break;
            case 'note.tags':
                setNoteTags(value);
                break;
            default:
                console.error(`No change handler registered for ${type}`);
        }

		
	};

    const handleDarkMode = () => {
		setDarkMode((current) => !current);
	};

    const handleSave: ThreeStringParamsFunc = (text, title, tags) => {
		if (!selectedNote?.id) {
			if (text.trim().length > 0) {
				const noteObj = {
                    title,
					text,
					date: new Date(),
					id: nanoid(),
                    tags: toStringArray(tags)
				};
				setNotes((current) => [...current, noteObj]);
			}

            clearNoteEditing();
		} else {
			updateNote(selectedNote);
		}
	};

	const handleEdit: StringFunc = (id) => {
		const selectedNote = notes.find((item) => item.id === id);
        if (!selectedNote) return;
        setSelectedNote(selectedNote);
        setNoteText(selectedNote.text);
        setNoteTitle(selectedNote.title);
        setNoteTags((selectedNote.tags || []).join(','));
	};

    const handleCancel: () => void = () => {
        setSelectedNote(null);
        clearNoteEditing();
    }

	const updateNote = (update: Note) => {
		const editedNote: Note = { ...update, text: noteText, title: noteTitle, tags: toStringArray(noteTags), date: new Date()  };
		if (editedNote.text.trim().length > 0) {
            // When updating, remove the prior entry if exist
            // Then add updated entry
			setNotes((current) => current.filter((item) => item.id !== update.id).concat(editedNote));
            clearNoteEditing();
		}

        setSelectedNote(null);
	};

	const handleDelete: StringFunc = (id) => {
		const filteredNotes = notes.filter((item) => item.id !== id);
		setNotes(filteredNotes);
	};

	const handleSearchText: StringFunc = (value) => {
		let newSearchText = value;
		if (newSearchText !== " ") {
			setSearchtext(newSearchText);
		}
	};

    const handleDataExport = () => {
        downloadFile(JSON.stringify(notes), 'Cocus Notes Export.json', 'text/json');
    };

    const downloadFile: DownloadFileFn = ( data, fileName, fileType ) => {
        // Create a blob with the data we want to download as a file
        const blob = new Blob([data], { type: fileType })
        // Create an anchor element and dispatch a click event on it
        // to trigger a download
        const a = document.createElement('a')
        a.download = fileName
        a.href = window.URL.createObjectURL(blob)
        const clickEvt = new MouseEvent('click', {
          view: window,
          bubbles: true,
          cancelable: true,
        })
        a.dispatchEvent(clickEvt)
        a.remove()
      }

    const handleDataImport = (files: FileList | null) => {
        if (!files) return;

        for (const file of Array.from(files)) {
            console.log(file);

            const reader = new FileReader()
            reader.onload = () => {
                const importText = reader.result?.toString();
                if (!importText) {
                    alert('Import file specified is empty');
                    return;
                }

                try {
                    const importData = JSON.parse(importText);
                    setNotes(importData);
                } catch (error) {
                    console.log('Failed to parse input file');
                    alert('Import file specified is bugged');
                }
            }
            
            reader.readAsText(file);
        }
    };


    return (
        <Context.Provider value={{
            darkMode,
            setDarkMode,
            handleDarkMode,
            handleChange,
            handleDelete,
            handleSave,
            handleEdit,
            handleCancel,
            handleSearchText,
            handleDataExport,
            handleDataImport,
            notes,
            noteText,
            noteTitle,
            noteTags,
            searchText,
            characterCount: MAX_CHARACTER_COUNT,
            selectedNote
        }}>
            { children }
        </Context.Provider>
    );
};

export default Context;
