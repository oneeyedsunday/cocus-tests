## Notes app

The basic functionality of the app is built off [this learning project by my younger brother](https://github.com/idiakeg/Notes) which I helped build.

The project itself is based off a tutorial off Youtube; the colorNote androi app and some improvements we both worked to build.


### Bonus

- [x] Add a Docker File.
- [x] Add Markdown Support.
- [x] Add note tagging or grouping
- [ ] ~~Add possibility to link notes~~
- [x] Search notes
- [x] Import / export all notes from storage
- [ ] ~~Add printable version~~

Obrigado ;)

### Sample Input for Import

A sample import file is provided [here](data/Cocus_Notes_Export.json)

