module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  moduleNameMapper: {
    "@modules/(.*)": "<rootDir>/src/modules/$1",
    "@shared/(.*)": "<rootDir>/src/shared/$1",
    "@config": "<rootDir>/src/config",
  },
  setupFiles: ["<rootDir>/test-setup.js"],
  transformIgnorePatterns: [
  ],
  "transform": {
    "^.+\\.(ts|tsx)$": "ts-jest",
    "\\.[j]sx?$": [
      "babel-jest",
      {
        "babelrc": false,
        "presets": ["@babel/preset-typescript"],
        "plugins": [
          "@babel/plugin-proposal-optional-chaining",
          "@babel/plugin-transform-modules-commonjs"
        ]
      }
    ]
  }
};
  