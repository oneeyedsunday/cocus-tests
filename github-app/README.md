
### Github App
The github app is a nodejs app written in typescript.
For best practices we will implement the following:

- Logging
- Dependency injection
- [x] Typescript (because i dont like getting my fingers burnt)
- [x] Some level of DDD
- Tests (especially to show the usefulness of DI)
- [x] Containerize the app (with docker)
- [x] Expose API documentation (perhaps with Swagger)
- [x] E2E Tests against actual github api
- Code is well commented
- [x] Minimal Rate Limiting
- [x] Caching
- [ ] Metrics (Just request handling time)



Obrigado  😅