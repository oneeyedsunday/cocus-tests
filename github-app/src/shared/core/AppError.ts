import { LoggerImpl } from "@shared/infrastructure/logger";
import { Result } from "./Result";
import { UseCaseError } from "./UseCaseError";

export class UnexpectedError extends Result<UseCaseError> {
  public constructor(err: string | Error) {
    super(false, {
      message: `An unexpected error occurred.`,
      error: err,
    } as UseCaseError);
    const logger = LoggerImpl;
    logger.error(`[AppError]: An unexpected error occurred: %O`, err);
  }

  public static create(err: string | Error): UnexpectedError {
    return new UnexpectedError(err);
  }
}
export class GithubUserNotFoundError extends Result<UseCaseError> {
  public constructor(username: string) {
    super(false, {
      message: `User ${username} not found on Github`,
    });

    const logger = LoggerImpl;
    logger.error(`[AppError]: Github user not found: %s`, username);
  }
}
