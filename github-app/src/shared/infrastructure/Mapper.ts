export interface Mapper<T, U> {
  toDTO(from: T): U;
}
