export interface WriteAsideCache<TResult, TQueryOpts> {
    getData(options: TQueryOpts): Promise<TResult | null>;
    setData(value: TResult, options: TQueryOpts): Promise<void>;
}