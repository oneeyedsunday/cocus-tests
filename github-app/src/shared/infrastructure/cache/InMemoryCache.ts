import NodeCache from "node-cache";
import { WriteAsideCache } from "./Cache";

export abstract class InMemoryWriteAsideCache<DataT, SeekT>
  implements WriteAsideCache<DataT, SeekT>
{
  protected readonly keyFactory: (options: SeekT) => string;
  protected readonly cache: NodeCache;
  /**
   * ttl in Milliseconds
   */
  protected readonly ttl: number;
  constructor(
    ttl: number,
    keyFactory: (options: SeekT) => string
  ) {
    this.ttl = ttl;
    this.cache = new NodeCache({ stdTTL: ttl * 1000, useClones: false });
    this.keyFactory = keyFactory;
  }

  abstract getData(options: SeekT): Promise<DataT | null>;
  abstract setData(value: DataT, options: SeekT): Promise<void>;

  protected getAsync(key: string): Promise<DataT | null> {
    const result  = this.cache.get<DataT>(key);

    return Promise.resolve(result || null);
  }

  protected setAsync(key: string, data: DataT): Promise<void> {
    return new Promise((resolve, reject) => {
        this.cache.set(key, data);
        resolve();
    });
  }
}