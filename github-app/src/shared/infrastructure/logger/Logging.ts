import logging, { IDebugger } from "debug";
// const logging = require("debug");

class Logging {
  name: string;
  stdout: IDebugger;
  stderr: IDebugger;

  constructor(name: string, pathPrefix = "app") {
    this.name = name;
    this.stderr = logging(`${pathPrefix}:${name}`);
    this.stdout = logging(`${pathPrefix}:${name}`);

    // log to stdout
    // eslint-disable-next-line no-console
    this.stdout.log = console.log.bind(console);

    this.debug = this.stdout.extend("DEBUG");
    this.warn = this.stderr.extend("WARN ");
    this.error = this.stderr.extend("ERROR");
    this.info = this.stdout.extend("INFO ");
  }

  addPrefix(prefix: string): void {
    this.debug = this.overwriteTemplate(this.debug, prefix);
    this.info = this.overwriteTemplate(this.info, prefix);
    this.warn = this.overwriteTemplate(this.warn, prefix);
    this.error = this.overwriteTemplate(this.error, prefix);
  }

  set(name: string): void {
    this.name = name;
  }

  debug(...msg: any[]): void {
    this.debug(...msg);
  }

  warn(...msg: any[]): void {
    this.warn(...msg);
  }

  error(...msg: any[]): void {
    this.error(...msg);
  }

  info(...msg: any[]): void {
    this.info(...msg);
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  private overwriteTemplate(origin: Function, prefix: string) {
    return (...msg: any[]) => {
      if (prefix && typeof msg[0] === "string") {
        msg[0] = `${prefix} ${msg[0]}`;
      } else if (prefix) {
        msg.unshift(prefix);
      }

      return origin(...msg);
    };
  }
}

export default Logging;
