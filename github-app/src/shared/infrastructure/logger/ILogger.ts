type LogFunc = (message: string | Error) => void;

export interface Logger {
  error: LogFunc;
  info: LogFunc;
  debug: LogFunc;
}
