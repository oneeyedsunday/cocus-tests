import express from "express";
import { BaseController } from "@shared/infrastructure/http/controllers/Base";

const healthCheckRouter = express.Router();

class HealthController extends BaseController {
    async executeImpl(_: express.Request, res: express.Response): Promise<any> {
        return this.ok(res, `App is up. Version: ${process.env.npm_package_version}. Environment: ${process.env.NODE_ENV || 'development'}`);
    }
}

const controller = new HealthController();

healthCheckRouter.use((req, res) => controller.execute(req, res));

export { healthCheckRouter };
