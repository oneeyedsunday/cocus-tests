import request from "supertest";
import * as express from "express";
import { bootstrapApp } from "../../../http/bootstrapApp";
import { StatusCodes } from "http-status-codes";

let app: express.Application;

beforeAll(() => {
  app = bootstrapApp();
});

describe("App supports application/json", () => {
  it("should support application/json", (done) => {
    request(app)
      .get("/api")
      .set("Content-Type", "application/json")
      .expect("Content-Type", /application\/json*/)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.statusCode).not.toBe(StatusCodes.NOT_ACCEPTABLE);
        done();
      });
  });

  it("should support variants of application/json", (done) => {
    request(app)
      .get("/api")
      .set("Content-Type", "application/json; charset=utf-8")
      .expect("Content-Type", /application\/json*/)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.statusCode).not.toBe(StatusCodes.NOT_ACCEPTABLE);
        done();
      });
  });
  it("should reject application/xml", (done) => {
    request(app)
      .get("/api")
      .set("Content-Type", "application/xml")
      .end((err, res) => {
        if (err) return done(err);
        expect(err).toBeDefined();
        expect(res.statusCode).toBe(StatusCodes.NOT_ACCEPTABLE);
        done();
      });
  });
  it("should reject application/text", (done) => {
    request(app)
      .get("/api")
      .set("Content-Type", "application/text")
      .expect(StatusCodes.NOT_ACCEPTABLE)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.statusCode).toBe(StatusCodes.NOT_ACCEPTABLE);
        done();
      });
  });
});
