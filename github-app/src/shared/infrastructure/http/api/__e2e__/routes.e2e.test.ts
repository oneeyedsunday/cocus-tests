import request from "supertest";
import * as express from "express";
import { bootstrapApp } from "../../../http/bootstrapApp";

let app: express.Application;

beforeAll(() => {
  app = bootstrapApp();
});

describe("App routing check", () => {
  it("should return json responses", (done) => {
    request(app)
      .get(`/api/non-existent-route`)
      .expect("Content-Type", /application\/json*/)
      .expect(404)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body).toMatchObject({
          message: "Resource /api/non-existent-route not found",
          status: 404,
        });
        done();
      });
  });
});
