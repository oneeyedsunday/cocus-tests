import express from "express";
import { searchRouter } from "@modules/search/infra/http/routes";
import RateLimiter from "../middlewares/RateLimiter";

const router = express.Router();

router.use("/search", RateLimiter, searchRouter);

export { router };
