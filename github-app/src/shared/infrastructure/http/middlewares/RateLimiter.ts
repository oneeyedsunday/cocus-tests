import express from "express";
import rateLimit from "express-rate-limit";
import { StatusCodes } from "http-status-codes";

const mins = Number(process.env.RATE_LIMIT_MINS || 5);
const maxRequests = Number(process.env.RATE_LIMIT_MAX_REQ || 5);

const RateLimiter = rateLimit({
  windowMs: mins * 60 * 1000,
  max: maxRequests,
  skipFailedRequests: true,
  // All users rate limited similarly
  // otherwise use req.ip
  keyGenerator: (req: express.Request) => "*",
  handler: (req: express.Request, res: express.Response) =>
    res.status(StatusCodes.TOO_MANY_REQUESTS).json({
      status: StatusCodes.TOO_MANY_REQUESTS,
      message: `Too Many Requests. Next window epoch: ${req.rateLimit.resetTime}`,
    }),
});

export default RateLimiter;
