import express from "express";
import { LoggerImpl } from "@shared/infrastructure/logger";
import { StatusCodes } from "http-status-codes";

const RejectNonApplicationJsonPayloads: MiddleWare = (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {
  const contentType =
    req.get("Content-Type") || req.headers["content-type"] || "";

  if (contentType.match(/application\/json*/)) return next();

  LoggerImpl.info(
    "Attempted request with unsupported content type: %s. request: %s %s",
    contentType,
    req.method,
    req.url
  );

  return res.status(StatusCodes.NOT_ACCEPTABLE).json({
    message: "We only support the application/json Content-Type and variants",
    status: StatusCodes.NOT_ACCEPTABLE,
  });
};

export default RejectNonApplicationJsonPayloads;
