import express from "express";
import { LoggerImpl } from "@shared/infrastructure/logger";

const defaultToJsonWhiteList = ["GET", "OPTIONS", "HEAD"];

const DefaultToApplicationJsonForNonPayloadRequests: MiddleWare = (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {
  const contentType =
    req.get("Content-Type") || req.headers["content-type"] || "";

  const method = req.method;

  const requestInDefaultToJsonWhiteList =
    defaultToJsonWhiteList.includes(method);

  if (!contentType && requestInDefaultToJsonWhiteList) {
    LoggerImpl.info(
      "Defaulting content-type of request %s to application/json: ",
      req.method,
      req.url
    );
    req.headers["content-type"] = "application/json";
  }

  next();
};

export default DefaultToApplicationJsonForNonPayloadRequests;
