import * as express from "express";
import { StatusCodes, ReasonPhrases } from "http-status-codes";

export class SendsResponse {
  public static jsonResponse(
    res: express.Response,
    code: number,
    message: string
  ): any {
    const isError = code >= StatusCodes.BAD_REQUEST;
    if (isError) return res.status(code).json({ status: code, message });
    return res.status(code).json({ message });
  }

  public ok<T>(res: express.Response, dto?: T): any {
    if (dto) {
      res.type("application/json");
      return res.status(StatusCodes.OK).json({ data: dto });
    } else {
      return res.status(StatusCodes.CREATED).json({});
    }
  }

  public clientError(res: express.Response, message?: string): any {
    return SendsResponse.jsonResponse(
      res,
      StatusCodes.BAD_REQUEST,
      message ? message : ReasonPhrases.BAD_REQUEST
    );
  }

  public unauthorized(res: express.Response, message?: string): any {
    return SendsResponse.jsonResponse(
      res,
      StatusCodes.UNAUTHORIZED,
      message ? message : ReasonPhrases.UNAUTHORIZED
    );
  }

  public notFound(res: express.Response, message?: string): any {
    return SendsResponse.jsonResponse(
      res,
      StatusCodes.NOT_FOUND,
      message ? message : ReasonPhrases.NOT_FOUND
    );
  }

  public notAcceptable(res: express.Response): any {
    return SendsResponse.jsonResponse(
      res,
      StatusCodes.NOT_ACCEPTABLE,
      ReasonPhrases.NOT_ACCEPTABLE
    );
  }

  public validationFailed(res: express.Response, message?: string): any {
    return SendsResponse.jsonResponse(
      res,
      StatusCodes.UNPROCESSABLE_ENTITY,
      message ? message : ReasonPhrases.UNPROCESSABLE_ENTITY
    );
  }

  public tooMany(res: express.Response, message?: string): any {
    return SendsResponse.jsonResponse(
      res,
      StatusCodes.TOO_MANY_REQUESTS,
      message ? message : ReasonPhrases.TOO_MANY_REQUESTS
    );
  }
}
