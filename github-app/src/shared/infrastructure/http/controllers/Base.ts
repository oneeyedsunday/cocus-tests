import * as express from "express";
import { LoggerImpl } from "@shared/infrastructure/logger";
import { Logger } from "../../logger/ILogger";
import { SendsResponse } from "./SendsResponse";
import { StatusCodes, ReasonPhrases } from "http-status-codes";

export abstract class BaseController extends SendsResponse {
  protected logger: Logger;
  constructor() {
    super();
    this.logger = LoggerImpl;
  }
  protected abstract executeImpl(
    req: express.Request,
    res: express.Response
  ): Promise<void | any>;

  public async execute(
    req: express.Request,
    res: express.Response
  ): Promise<void> {
    try {
      await this.executeImpl(req, res);
    } catch (err) {
      this.logger.info(
        `[${
          (Reflect.getPrototypeOf(this)?.constructor || this.constructor)
            ?.name || "BaseController"
        }]: Uncaught controller error`
      );
      this.logger.info(err as any);
      this.fail(res, "An unexpected error occurred");
    }
  }

  public fail(res: express.Response, error: Error | string): any {
    this.logger.info(error);
    return BaseController.jsonResponse(
      res,
      StatusCodes.INTERNAL_SERVER_ERROR,
      error instanceof Error
        ? error.message
        : ReasonPhrases.INTERNAL_SERVER_ERROR
    );
  }
}
