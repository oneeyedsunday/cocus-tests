import express from "express";
import { json } from "body-parser";
import { catchRouter } from "@shared/infrastructure/http/api/catch";
import { healthCheckRouter } from "@shared/infrastructure/http/api/health";
import { router } from "../http/api/routes";
import RejectNonApplicationJsonPayloads from "./middlewares/RejectNonApplicationJsonRequests";
import DefaultToApplicationJsonForNonPayloadRequests from "./middlewares/DefaultToApplicationJsonForNonPayloadRequests";

// Returns handler for server
function bootstrapApp(): express.Express {
  const app = express();

  app.use(DefaultToApplicationJsonForNonPayloadRequests);
  app.use(RejectNonApplicationJsonPayloads);
  app.use(json());

  app.use("/api", router);

  // Health check handler
  app.use("/health", healthCheckRouter);
  // 404 handler
  app.use(catchRouter);
  return app;
}

export { bootstrapApp };
