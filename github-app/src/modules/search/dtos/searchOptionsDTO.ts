type SearchOptionsDTO = {
  username: string;
};

export default SearchOptionsDTO;
