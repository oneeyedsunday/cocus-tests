export type GithubSearchResultsDTO = Array<GithubSearchResultDTO>;

export interface GithubSearchResultDTO {
  repositoryName: string;
  ownerUserName: string;
  branches: Array<{
    name: string;
    lastCommitSHA: string;
  }>;
}
