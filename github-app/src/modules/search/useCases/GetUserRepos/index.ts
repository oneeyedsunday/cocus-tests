import { client } from "@modules/search/clients";
import { githubResultsWriteAsideCache } from "@modules/search/infra/cache";
import { LoggerImpl } from "@shared/infrastructure/logger";
import { GetUserRepos } from "./GetUserRepos";
import { GetUserReposController } from "./GetUserReposController";

const getUserRepos = new GetUserRepos(client, githubResultsWriteAsideCache, LoggerImpl);
const getUserReposController = new GetUserReposController(getUserRepos);

export { getUserRepos, getUserReposController };
