import { IGithubClient } from "@modules/search/clients/githubClient";
import GithubRepoResult from "@modules/search/clients/models/GithubRepoResult";
import { Result, right, left, Either } from "@shared/core/Result";
import * as AppError from "@shared/core/AppError";
import { UseCase } from "@shared/core/UseCase";
import SearchOptionsDTO from "@modules/search/dtos/searchOptionsDTO";
import { GithubUserCache } from "@modules/search/infra/cache/GithubUser";
import Logging from "@shared/infrastructure/logger/Logging";

type Response = Either<AppError.UnexpectedError, Result<GithubRepoResult[]>>;

export class GetUserRepos
  implements UseCase<SearchOptionsDTO, Promise<Response>>
{
  private readonly client: IGithubClient;
  private readonly cache: GithubUserCache;
  private readonly logger: Logging;

  constructor(client: IGithubClient, ghResultsCache: GithubUserCache, logger: Logging) {
    this.client = client;
    this.cache = ghResultsCache;
    this.logger = logger;
  }

  public async execute(req: SearchOptionsDTO): Promise<Response> {
    try {
      const cachedResults = await this.cache.getData(req.username);

      if (cachedResults) {
        this.logger.info(`Found results for username: ${req.username} in cache`);
        return right(Result.ok<GithubRepoResult[]>(cachedResults));
      }
      const results = await this.client.getAllNonForkRepos(req.username);
      this.cache.setData(results, req.username).catch(err => {
        this.logger.error('Failed to cache aside: %o', err.message);
      });
      return right(Result.ok<GithubRepoResult[]>(results));
    } catch (err: any) {
      switch (err.code) {
        case 404:
          return left(new AppError.GithubUserNotFoundError(req.username));
        default:
          return left(new AppError.UnexpectedError(err as Error));
      }
    }
  }
}
