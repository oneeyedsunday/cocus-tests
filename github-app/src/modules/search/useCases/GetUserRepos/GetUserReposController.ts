import { BaseController } from "@shared/infrastructure/http/controllers/Base";
import express from "express";
import { GithubSearchResultMapper } from "@modules/search/mappers/githubSearchResult";
import { GithubSearchResultsDTO } from "@modules/search/dtos/githubSearchResultDTO";
import { GetUserRepos } from "./GetUserRepos";
import SearchOptionsDTO from "@modules/search/dtos/searchOptionsDTO";
import * as AppError from "@shared/core/AppError";

type ApiResponse = {
  results: GithubSearchResultsDTO;
};

export class GetUserReposController extends BaseController {
  private useCase: GetUserRepos;

  constructor(useCase: GetUserRepos) {
    super();
    this.useCase = useCase;
  }

  async executeImpl(
    req: express.Request,
    res: express.Response
  ): Promise<express.Response> {
    try {
      const dto: SearchOptionsDTO = {
        username: req.params.username,
      };
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;

        switch (error.constructor) {
          case AppError.GithubUserNotFoundError:
            return this.notFound(
              res,
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              (error as any as AppError.GithubUserNotFoundError).error.message
            );
          default:
            return this.fail(res, error.errorValue().message);
        }
      } else {
        const results = result.value.getValue();
        const mapper = new GithubSearchResultMapper();
        return this.ok<ApiResponse>(res, {
          results: mapper.toDTO(results),
        });
      }
    } catch (err) {
      return this.fail(res, err as Error);
    }
  }
}
