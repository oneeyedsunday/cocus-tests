import { Mapper } from "@shared/infrastructure/Mapper";
import GithubRepoResult from "../clients/models/GithubRepoResult";
import { GithubSearchResultsDTO } from "../dtos/githubSearchResultDTO";

export class GithubSearchResultMapper
  implements Mapper<GithubRepoResult[], GithubSearchResultsDTO>
{
  toDTO(from: GithubRepoResult[]): GithubSearchResultsDTO {
    return from.map((item) => ({
      repositoryName: item.name,
      ownerUserName: item.owner.login,
      branches: item.branches.map((branch) => ({
        name: branch.name,
        lastCommitSHA: branch.commit.sha,
      })),
    }));
  }
}
