import { IGithubClient } from "./githubClient";
import GithubRepoResult from "./models/GithubRepoResult";
import { LoggerImpl } from "@shared/infrastructure/logger";
import https from "https";
import { StatusCodes } from "http-status-codes";
import _ from "lodash";
import GithubBranchResult from "./models/GithubBranchResult";

function httpsGet<TResponse = any>(
  url: string,
  extraHeaders?: { [key: string]: string }
): Promise<{
  data: TResponse;
  headers: { [key: string]: string | string[] | undefined };
}> {
  return new Promise((resolve, reject) => {
    https
      .get(
        url,
        {
          agent: new https.Agent({ keepAlive: true }),
          timeout: 1000 * 5, // 5 seconds
          headers: {
            "Content-Type": "application/json",
            "User-Agent": "CoCus Portugal test App",
            ...(extraHeaders ? extraHeaders : {}),
          },
        },
        (res) => {
          let rawData = "";

          res.on("data", (chunk) => {
            rawData += chunk;
          });

          res.on("end", () => {
            if (res.statusCode && res.statusCode >= StatusCodes.BAD_REQUEST) {
              const err = new Error(res.statusMessage);
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              err.code = res.statusCode;
              return reject(err);
            }
            // console.log("rawData: ", rawData, res.statusCode);
            const parsedData = JSON.parse(rawData);
            resolve({ data: parsedData, headers: res.headers });
          });
        }
      )
      .once("error", (err) => reject(err));
  });
}

function getAuthHeader() {
  if (!process.env.GITHUB_TOKEN) return undefined;

  return {
    Authorization: `Bearer ${process.env.GITHUB_TOKEN}`,
  };
}

export class GithubV3RestApiClient implements IGithubClient {
  private static PER_PAGE = 100;
  // This should be less or equal to size of PER_PAGE for optimal performance
  private static REPO_ENRICH_BATCH_SIZE = 10;
  private readonly logger = LoggerImpl;

  async getAllNonForkRepos(username: string): Promise<GithubRepoResult[]> {
    const data: GithubRepoResult[] = [];
    let hasNext = false;
    let currentPage = 1;

    // call till header doesnt hold any pagination info
    do {
      const fetchResult = await this.fetchRepos(username, currentPage);
      hasNext = fetchResult.hasNext;
      data.push(...fetchResult.data);
      currentPage++;
    } while (hasNext);

    return data;
  }

  private async handleBranchEnrichChunk(
    repos: GithubRepoResult[]
  ): Promise<void> {
    await Promise.all(
      repos.map(async (repo) => {
        try {
          this.logger.info("Enriching repo: (%s)", repo.name);
          const ghResponse = await httpsGet<GithubBranchResult[]>(
            `https://api.github.com/repos/oneEyedSunday/${repo.name}/branches?per_page=${GithubV3RestApiClient.PER_PAGE}`,
            getAuthHeader()
          );

          repo.branches = ghResponse.data;
        } catch (error) {
          this.logger.error(
            "Failed to enrich repo: (%s) with branch data, because %s",
            repo.name,
            (error as Error).message
          );

          // Skip 404 errors for branches
          if ((error as any).code === 404) {
            repo.branches = [];
            return;
          }
          // Fail fast
          // Ideally we could impl. some retry logic
          // Usually the SDK would have that anyways
          throw error;
        }
      })
    );
  }

  private async enrichBranchInfoForRepos(
    repos: GithubRepoResult[]
  ): Promise<void> {
    // process chunks one at a time
    // Inside a chunk, i.e process BATCH_SIZE storeTokens in parallel
    for (const repoChunk of _.chunk(
      repos,
      GithubV3RestApiClient.REPO_ENRICH_BATCH_SIZE
    )) {
      // eslint-disable-next-line no-await-in-loop
      await this.handleBranchEnrichChunk(repoChunk);
    }
  }

  private async fetchRepos(
    userName: string,
    page = 1
  ): Promise<{ data: GithubRepoResult[]; hasNext: boolean }> {
    this.logger.info(
      "Fetching github repo data for %s at page %s",
      userName,
      page
    );
    try {
      const ghResponse = await httpsGet<GithubRepoResult[]>(
        `https://api.github.com/users/${userName}/repos?per_page=${GithubV3RestApiClient.PER_PAGE}&page=${page}&sort=updated`,
        getAuthHeader()
      );

      const linkHeaderValue = ghResponse.headers["link"];

      const hasNextLink =
        linkHeaderValue &&
        typeof linkHeaderValue === "string" &&
        linkHeaderValue.includes('rel="next",');

      // Will need to query branches everytime
      await this.enrichBranchInfoForRepos(ghResponse.data);

      return {
        data: ghResponse.data,
        hasNext: hasNextLink ? true : false,
      };
    } catch (err) {
      this.logger.error("An error occured fetching repos from github: %O", err);
      throw err;
    }
  }
}
