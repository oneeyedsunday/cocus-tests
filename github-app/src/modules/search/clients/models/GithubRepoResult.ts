import GithubBranchResult from "./GithubBranchResult";

type GithubRepoOwner = {
  login: string;
  id: number;
  avatar_url: string;
  gravatar_id: string;
  url: string;
  html_url: string;
  followers_url: string;
  following_url: string;
  gists_url: string;
  starred_url: string;
  subscriptions_url: string;
  organizations_url: string;
  repos_url: string;
  events_url: string;
  received_events_url: string;
  type: "User" | string;
  site_admin: boolean;
};

/**
 * @description Date as a string in format  "YYYY-MM-DDTHH:MM:ssZ"
 */
type GithubDateString = string;

type GithubRepoResult = {
  id: number;
  name: string;
  full_name: string;
  private: boolean;
  owner: GithubRepoOwner;
  html_url: string;
  description: string;
  fork: boolean;
  url: string;
  forks_url: string;
  keys_url: string;
  collaborators_url: string;
  teams_url: string;
  hooks_url: string;
  issue_events_url: string;
  assignees_url: string;
  branches_url: string;
  commits_url: string;
  git_commits_url: string;
  issues_url: string;
  pulls_url: string;
  deployments_url: string;
  created_at: GithubDateString;
  updated_at: GithubDateString;
  pushed_at: GithubDateString;
  git_url: string;
  ssh_url: string;
  clone_url: string;
  svn_url: string;
  homepage: string | null;
  size: number;
  stargazers_count: number;
  watchers_count: number;
  language: string;
  has_issues: boolean;
  has_projects: boolean;
  has_downloads: boolean;
  has_wiki: boolean;
  has_pages: boolean;
  forks_count: number;
  mirror_url: string;
  archived: boolean;
  disabled: boolean;
  open_issues_count: number;
  license: string;
  allow_forking: boolean;
  is_template: boolean;
  web_commit_signoff_required: boolean;
  topics: Array<string>;
  visibility: "public" | "private";
  forks: 0;
  open_issues: 0;
  watchers: 0;
  default_branch: string;
  branches: GithubBranchResult[];
};

export default GithubRepoResult;
