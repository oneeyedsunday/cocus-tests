type GithubBranchResult = {
  name: string;
  commit: {
    sha: string;
    url: string;
  };
  protected: boolean;
};

export default GithubBranchResult;
