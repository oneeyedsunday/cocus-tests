import { GithubV3RestApiClient } from "./GithubV3RestApiClient";

const client = new GithubV3RestApiClient();

export { client };
