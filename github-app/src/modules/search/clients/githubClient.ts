import GithubRepoResult from "./models/GithubRepoResult";

export interface IGithubClient {
  getAllNonForkRepos(username: string): Promise<Array<GithubRepoResult>>;
}
