import GithubRepoResult from "@modules/search/clients/models/GithubRepoResult";
import { WriteAsideCache } from "@shared/infrastructure/cache/Cache";
import { InMemoryWriteAsideCache } from "@shared/infrastructure/cache/InMemoryCache";

export class GithubUserCache extends InMemoryWriteAsideCache<GithubRepoResult[], string> implements WriteAsideCache<GithubRepoResult[], string> {
  public async getData(
    options: string
  ): Promise<GithubRepoResult[] | null> {
    try {
      const items = await this.getAsync(this.keyFactory(options));
      if (!items) return null;
    return items;
    } catch (err) {
      return null;
    }
  }
  public async setData(
    value: GithubRepoResult[],
    options: string
  ): Promise<void> {
    await this.setAsync(this.keyFactory(options), value);
  }
}