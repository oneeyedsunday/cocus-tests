import { GithubUserCache } from "./GithubUser";

const ttl = Number(process.env.CACHE_TTL_SECONDS || 30);

const githubResultsWriteAsideCache = new GithubUserCache(
    ttl,
    (userName: string) => {
      return `ghResults-${userName}`;
    }
);
  
export { githubResultsWriteAsideCache };
