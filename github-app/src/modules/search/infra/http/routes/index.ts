import { getUserReposController } from "@modules/search/useCases/GetUserRepos";
import express from "express";

const searchRouter = express.Router();

searchRouter.get("/github/:username", (req, res) =>
  getUserReposController.execute(req, res)
);

export { searchRouter };
