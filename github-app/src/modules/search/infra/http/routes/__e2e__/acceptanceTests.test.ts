import request from "supertest";
import * as express from "express";
import { bootstrapApp } from "@shared/infrastructure/http/bootstrapApp";
import { StatusCodes } from "http-status-codes";

let app: express.Application;

beforeAll(() => {
  app = bootstrapApp();
});

function validateSuccessResponseBody(
  body: { [key: string]: any },
  repoLength: number
) {
  expect(body).toHaveProperty("data");
  expect(body.data).toHaveProperty("results");
  expect(Array.isArray(body.data.results)).toBeTruthy();
  expect(body.data.results.length).toBe(repoLength);
  body.data.results.forEach((repo: { branches: any[] }) => {
    // ~~should have at least a branch~~
    // Not always true
    // see https://github.com/oneEyedSunday/react-auth0-auth-hooks
    expect(repo.branches).toBeDefined();
  });
}

describe("Acceptance Tests (E2E))", () => {
  it.skip("should find repos for bob", () => {
    jest.setTimeout(900000);
    return request(app)
      .get("/api/search/github/bob")
      .set("Content-Type", "application/json")
      .expect(StatusCodes.OK)
      .then((res) => {
        validateSuccessResponseBody(res.body, 12);
      });
  });

  it("should find no repos for ghost", () => {
    return request(app)
      .get("/api/search/github/ghost")
      .set("Content-Type", "application/json")
      .expect(StatusCodes.OK)
      .then((res) => {
        validateSuccessResponseBody(res.body, 0);
      });
  });

  it("should throw 404 for oneeyedsunday-definitely-not-a-user", () => {
    return request(app)
      .get("/api/search/github/oneeyedsunday-definitely-not-a-user")
      .set("Content-Type", "application/json")
      .expect(StatusCodes.NOT_FOUND)
      .then((res) => {
        expect(res.body).toMatchObject({
          status: 404,
          message:
            "User oneeyedsunday-definitely-not-a-user not found on Github",
        });
      });
  });
});
