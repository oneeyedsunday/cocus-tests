declare namespace NodeJS {
    export interface ProcessEnv {
      PORT: number | string;
      GITHUB_TOKEN: string;
      RATE_LIMIT_MINS: number;
      RATE_LIMIT_MAX_REQ: number;
      CACHE_TTL_SECONDS: number;
      NODE_ENV: string;
      npm_package_version: string;
    }
}